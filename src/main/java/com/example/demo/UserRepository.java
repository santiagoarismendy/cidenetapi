package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    // I'm sorry I didn't have time to make validations as Email, doc number, generate email
}
