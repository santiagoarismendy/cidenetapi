package com.example.demo;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.*;

@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"tipo_identificacion", "numero_identificacion"})
})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "El primer apellido es requerido")
    @Size(max = 20, message = "El primer apellido debe tener máximo 20 caracteres")
    @Pattern(regexp = "^[A-Z]*$", message = "El primer apellido debe contener solo caracteres de la A a la Z mayúsculas, sin acentos ni Ñ")
    @Column(name = "primer_apellido")
    private String primerApellido;

    @NotBlank(message = "El segundo apellido es requerido")
    @Size(max = 20, message = "El segundo apellido debe tener máximo 20 caracteres")
    @Pattern(regexp = "^[A-Z]*$", message = "El segundo apellido debe contener solo caracteres de la A a la Z mayúsculas, sin acentos ni Ñ")
    @Column(name = "segundo_apellido")
    private String segundoApellido;

    @NotBlank(message = "El primer nombre es requerido")
    @Size(max = 20, message = "El primer nombre debe tener máximo 20 caracteres")
    @Pattern(regexp = "^[A-Z]*$", message = "El primer nombre debe contener solo caracteres de la A a la Z mayúsculas, sin acentos ni Ñ")
    @Column(name = "primer_nombre")
    private String primerNombre;

    @Size(max = 50, message = "Los otros nombres deben tener máximo 50 caracteres")
    @Pattern(regexp = "^[A-Z ]*$", message = "Los otros nombres deben contener solo caracteres de la A a la Z mayúsculas, sin acentos ni Ñ y el caracter espacio entre nombres")
    @Column(name = "otros_nombres")
    private String otrosNombres;

    @NotBlank(message = "El país del empleo es requerido")
    @Column(name = "pais_empleo")
    private String paisEmpleo;

    @NotBlank(message = "El tipo de identificación es requerido")
    @Column(name = "tipo_identificacion")
    private String tipoIdentificacion;

    @NotBlank(message = "El número de identificación es requerido")
    @Size(max = 20, message = "El número de identificación debe tener máximo 20 caracteres")
    @Column(name = "numero_identificacion")
    private String numeroIdentificacion;

    @Email(message = "El correo electrónico debe tener un formato válido")
    @Size(max = 300, message = "El correo electrónico debe tener máximo 300 caracteres")
    @Column(unique = true)
    private String email;

    @NotNull(message = "La fecha de ingreso es requerida")
    @Column(name = "fecha_ingreso")
    private LocalDate fechaIngreso;

    @NotBlank(message = "El área es requerido")
    private String area;

    @Column(name = "estado")
    private String estado = "Activo";

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro = LocalDateTime.now();

    // Constructor vacioo
    public User() {

    }

    // Constructor con parametros
    public User(String primerApellido, String segundoApellido, String primerNombre, String otrosNombres,
                String paisEmpleo, String tipoIdentificacion, String numeroIdentificacion, String email,
                LocalDate fechaIngreso, String area) {
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.primerNombre = primerNombre;
        this.otrosNombres = otrosNombres;
        this.paisEmpleo = paisEmpleo;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.email = email;
        this.fechaIngreso = fechaIngreso;
        this.area = area;
    }

    // getters y etters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getOtrosNombres() {
        return otrosNombres;
    }

    public void setOtrosNombres(String otrosNombres) {
        this.otrosNombres = otrosNombres;
    }

    public String getPaisEmpleo() {
        return paisEmpleo;
    }

    public void setPaisEmpleo(String paisEmpleo) {
        this.paisEmpleo = paisEmpleo;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}