-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-03-2023 a las 04:39:40
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cidenet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `primer_apellido` varchar(20) NOT NULL,
  `segundo_apellido` varchar(20) NOT NULL,
  `primer_nombre` varchar(20) NOT NULL,
  `otros_nombres` varchar(50) DEFAULT NULL,
  `pais_empleo` varchar(255) NOT NULL,
  `tipo_identificacion` varchar(255) NOT NULL,
  `numero_identificacion` varchar(20) NOT NULL,
  `email` varchar(300) DEFAULT NULL,
  `fecha_ingreso` date NOT NULL,
  `area` varchar(255) NOT NULL,
  `estado` varchar(255) DEFAULT 'Activo',
  `fecha_registro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `primer_apellido`, `segundo_apellido`, `primer_nombre`, `otros_nombres`, `pais_empleo`, `tipo_identificacion`, `numero_identificacion`, `email`, `fecha_ingreso`, `area`, `estado`, `fecha_registro`) VALUES
(1, 'GARCIA', 'PEREZ', 'JUAN', 'CARLOS', 'Colombia', 'Cédula de Ciudadanía', '123456789', 'juan.garcia@example.com', '2022-03-15', 'Desarrollo', 'Activo', '2023-03-16 02:51:05'),
(6, 'GOMEZ', 'LOPEZ', 'KEVIN', 'SANTIAGO', 'Colombia', 'Cédula de Ciudadanía', '123452789', 'juans.garcia@example.com', '2022-03-15', 'Desarrollo', 'Activo', '2023-03-16 03:08:08'),
(11, 'LOPEZZZZZZ', 'SIERRA', 'KEVIN', 'SANTIAGO', 'Colombia', 'Cédula de Ciudadanía', '223852789', 'kevin.garcia@example.com', '2022-03-15', 'Desarrollo', 'Activo', '2023-03-16 03:14:25');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_users_tipo_identificacion_numero_identificacion` (`tipo_identificacion`,`numero_identificacion`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
